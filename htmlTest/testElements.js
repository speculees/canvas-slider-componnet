function createTestElements(){
  var container = $('.container');
  container.append('<div class="item1 start"/>');
  container.append('<div class="item2 start"/>');
  container.append('<div class="item3 start"/>');
  container.append('<div class="item4 start"/>');
  container.append('<div class="item5 start"/>');
  $('.item1').css({
    'top' : '200px',
    'left' : '187px',
    'width': '70px',
    'height': '40px',
//    'transition' : '.4s linear',

  });
  $('.item2').css({ 
    'top' : '250px',
    'width': '40px',
    'height': '40px',
    'border-radius' : '40px'
  });
  $('.item3').css({ 
    'top' : '185px',
    'width': '70px',
    'height': '70px',
    'border-radius' : '40px'
  });
  $('.item4').css({ 
    'width': '20px',
    'height': '20px',
 });
  $('.item5').css({ 
    'left' : '700px',
    'top' : '156px',
    'width': '700px',
    'height': '40px',
 });
  $('.start').css({
    'visibility' : 'hidden',
    'background-color' : 'green',
    'position' : 'relative'
  });
  //CSS ANIMATE 
  setTimeout(function(){ 
   $('.item1').animate(
     {
     visibility : 'visible',
     top: '260px'
     }, 500, 'easeOutBack');
   $('.item2').animate(
     { 
    'left' : '234px',
     visibility : 'visible'
     }, 500);
   $('.item3').animate(
     { 
     'left' : '144px', 
     visibility : 'visible'
    }, 500);
   $('.item4').animate(
     {
    'top' : '100px',
    'left' : '168px',
     visibility : 'visible'
    }, 500);
   $('.item5').animate(
     {
      left : '0px',
     visibility : 'visible'
    }, 500);
  }, 2000);
}
