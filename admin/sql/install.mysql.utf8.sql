SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- -----------------------------------------------------
-- Table `#__CANVAS_SLIDER`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__CANVAS_SLIDER` ;

CREATE  TABLE IF NOT EXISTS `#__CANVAS_SLIDER` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL ,
  `created` TIMESTAMP default NOW(),
  `width` INT NOT NULL,
  `height` INT NOT NULL,
  `responsive` BOOLEAN default TRUE,
  `fullscreen` BOOLEAN default FALSE,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;
INSERT INTO `#__CANVAS_SLIDER` (`name`, `width`, `height`) VALUES ('slider1', 640, 480);
INSERT INTO `#__CANVAS_SLIDER` (`name`, `width`, `height`) VALUES ('slider2', 640, 480);


-- -----------------------------------------------------
-- Table `#__CANVAS_SLIDER_SLIDE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__CANVAS_SLIDER_SLIDE` ;

CREATE  TABLE IF NOT EXISTS `#__CANVAS_SLIDER_SLIDE` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL ,
  `time` INT NOT NULL ,
  `cue` INT NOT NULL,
  `CANVAS_SLIDER_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_CANVAS_SLIDER_SLIDE_CANVAS_SLIDER` (`CANVAS_SLIDER_id` ASC),
  CONSTRAINT `fk_CANVAS_SLIDER_SLIDE_CANVAS_SLIDER`
    FOREIGN KEY (`CANVAS_SLIDER_id` )
    REFERENCES `#__CANVAS_SLIDER` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
INSERT INTO `#__CANVAS_SLIDER_SLIDE` (`name`, `time`, `CANVAS_SLIDER_id`, `cue`) VALUES ('slide1', 5, 1, 1);
INSERT INTO `#__CANVAS_SLIDER_SLIDE` (`name`, `time`, `CANVAS_SLIDER_id`, `cue`) VALUES ('slide2', 10, 1, 2);
INSERT INTO `#__CANVAS_SLIDER_SLIDE` (`name`, `time`, `CANVAS_SLIDER_id`, `cue`) VALUES ('slide3', 5, 2, 1);
INSERT INTO `#__CANVAS_SLIDER_SLIDE` (`name`, `time`, `CANVAS_SLIDER_id`, `cue`) VALUES ('slide4', 10, 2, 2);

-- -----------------------------------------------------
-- Table `#__CANVAS_SLIDER_ELEMENT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__CANVAS_SLIDER_ELEMENT` ;

CREATE  TABLE IF NOT EXISTS `#__CANVAS_SLIDER_ELEMENT` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL ,
  `content` VARCHAR(45) NULL ,
  `alt` VARCHAR(45) NULL ,
  `src` VARCHAR(45) NULL ,
  `cue` INT,
  `type` CHAR(1) default 'i',
  `CANVAS_SLIDER_SLIDE_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_CANVAS_SLIDER_IMG_CANVAS_SLIDER_SLIDE1` (`CANVAS_SLIDER_SLIDE_id` ASC) ,
  CONSTRAINT `fk_CANVAS_SLIDER_IMG_CANVAS_SLIDER_SLIDE1`
    FOREIGN KEY (`CANVAS_SLIDER_SLIDE_id` )
    REFERENCES `#__CANVAS_SLIDER_SLIDE` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `#__CANVAS_SLIDER_ELEMENT` (`name`, `type`, `CANVAS_SLIDER_SLIDE_id`) values ('image element 1', 'i', 1);
INSERT INTO `#__CANVAS_SLIDER_ELEMENT` (`name`, `type`, `CANVAS_SLIDER_SLIDE_id`) values ('image element 2', 'i', 1);
INSERT INTO `#__CANVAS_SLIDER_ELEMENT` (`name`, `type`, `CANVAS_SLIDER_SLIDE_id`) values ('text element 1', 't', 1);
INSERT INTO `#__CANVAS_SLIDER_ELEMENT` (`name`, `type`, `CANVAS_SLIDER_SLIDE_id`) values ('text element 2', 't', 1);
INSERT INTO `#__CANVAS_SLIDER_ELEMENT` (`name`, `type`, `CANVAS_SLIDER_SLIDE_id`) values ('canvas element 1', 'c', 1);
INSERT INTO `#__CANVAS_SLIDER_ELEMENT` (`name`, `type`, `CANVAS_SLIDER_SLIDE_id`) values ('canvas element 2', 'c', 1);

-- -----------------------------------------------------
-- Table `#__CANVAS_SLIDER_CSS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__CANVAS_SLIDER_CSS` ;

CREATE  TABLE IF NOT EXISTS `#__CANVAS_SLIDER_CSS` (
  `id` INT AUTO_INCREMENT,
  `background` VARCHAR(6),
  `width` INT,
  `height` INT,
  `font` VARCHAR(100),
  `shadow` VARCHAR(30),
  `rotate` INT,
  `up` INT,
  `down` INT,
  `left` INT,
  `right` INT,
  `element_id` INT DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_CANVAS_SLIDER_CSS_CANVAS_SLIDER_ELEMENT1` (`element_id` ASC) ,
  CONSTRAINT `fk_CANVAS_SLIDER_CSS_CANVAS_SLIDER_ELEMENT1`
    FOREIGN KEY (`element_id` )
    REFERENCES `#__CANVAS_SLIDER_ELEMENT` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `#__CANVAS_SLIDER_CSS` (`background`, `width`, `height`, `shadow`, `rotate`, `up`, `down`, `left`, `right`, `element_id`) VALUES ('0A0', 30, 30, '10px 10px 5px #888888',
  NULL, 40,0,0,0,1 );
INSERT INTO `#__CANVAS_SLIDER_CSS` (`background`, `width`, `height`, `shadow`, `rotate`, `up`, `down`, `left`, `right`, `element_id`) VALUES ('00F', 30, 30, '10px 10px 5px #888888',
  NULL, 80,40,0,0,2 );
INSERT INTO `#__CANVAS_SLIDER_CSS` (`background`, `width`, `height`, `shadow`, `rotate`, `up`, `down`, `left`, `right`, `element_id`) VALUES ('0A0', 30, 30, '10px 10px 5px #888888',
  NULL, 40,100,0,0,3 );
INSERT INTO `#__CANVAS_SLIDER_CSS` (`background`, `width`, `height`, `shadow`, `rotate`, `up`, `down`, `left`, `right`, `element_id`) VALUES ('00F', 30, 30, '10px 10px 5px #888888',
  NULL, 80,40,100,0,4 );
INSERT INTO `#__CANVAS_SLIDER_CSS` (`background`, `width`, `height`, `shadow`, `rotate`, `up`, `down`, `left`, `right`, `element_id`) VALUES ('0A0', 30, 30, '10px 10px 5px #888888',
  NULL, 140,0,0,20,5 );
INSERT INTO `#__CANVAS_SLIDER_CSS` (`background`, `width`, `height`, `shadow`, `rotate`, `up`, `down`, `left`, `right`, `element_id`) VALUES ('00F', 30, 30, '10px 10px 5px #888888',
  NULL, 280,40,20,0,6 );

-- -----------------------------------------------------
-- Table `#__CANVAS_SLIDER_TRANSITIONS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__CANVAS_SLIDER_TRANSITIONS` ;

CREATE  TABLE IF NOT EXISTS `#__CANVAS_SLIDER_TRANSITIONS` (
  `id` INT AUTO_INCREMENT,
  `up` BOOLEAN default FALSE,
  `down` BOOLEAN default FALSE,
  `left` BOOLEAN default FALSE,
  `right` BOOLEAN default FALSE,
  `fxTransition` VARCHAR(20),
  `fxEase` VARCHAR(10),
  `duration` INT,
  `delay` INT,
  `element_id` INT DEFAULT NULL,
  PRIMARY KEY (`id`) ,
  INDEX `fk_CANVAS_SLIDER_TRANSITIONS_CANVAS_SLIDER_ELEMENT1` (`element_id` ASC) ,
  CONSTRAINT `fk_CANVAS_SLIDER_TRANSITIONS_CANVAS_SLIDER_ELEMENT1`
    FOREIGN KEY (`element_id` )
    REFERENCES `#__CANVAS_SLIDER_ELEMENT` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `#__CANVAS_SLIDER_TRANSITIONS` (`up`, `fxTransition`, `fxEase`, `duration`, `delay`, `element_id`) VALUES (TRUE, 'linear', 'easeIn', 1000, 1000, 1);

INSERT INTO `#__CANVAS_SLIDER_TRANSITIONS` (`down`, `fxTransition`, `fxEase`, `duration`, `delay`, `element_id`) VALUES (TRUE, 'Elastic', 'easeIn', 2000, 1000, 2);

INSERT INTO `#__CANVAS_SLIDER_TRANSITIONS` (`up`, `fxTransition`, `fxEase`, `duration`, `delay`, `element_id`) VALUES (TRUE, 'linear', 'easeIn', 1000, 1000, 3);

INSERT INTO `#__CANVAS_SLIDER_TRANSITIONS` (`down`, `fxTransition`, `fxEase`, `duration`, `delay`, `element_id`) VALUES (TRUE, 'Elastic', 'easeIn', 2000, 1000, 4);

INSERT INTO `#__CANVAS_SLIDER_TRANSITIONS` (`up`, `fxTransition`, `fxEase`, `duration`, `delay`, `element_id`) VALUES (TRUE, 'linear', 'easeIn', 1000, 1000, 5);

INSERT INTO `#__CANVAS_SLIDER_TRANSITIONS` (`down`, `fxTransition`, `fxEase`, `duration`, `delay`, `element_id`) VALUES (TRUE, 'Elastic', 'easeIn', 2000, 1000, 6);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

