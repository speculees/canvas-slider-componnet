<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
/**
 * CanvasSlider View
 */
class CanvasSliderViewElementCrud extends JViewLegacy
{
        /**
         * display method of Hello view
         * @return void
         */
        public function display($tpl = null) 
        {
        		$document = JFactory::getDocument();
				$document -> addStyleSheet('components/com_canvasslider/css/position.css');
				$document -> addStyleSheet('components/com_canvasslider/css/chooser.css');
				$document -> addCustomTag('<script type="text/javascript" src="'.JURI::base().'components/com_canvasslider/js/position.js"></script>');
				$document -> addCustomTag('<script type="text/javascript" src="'.JURI::base().'components/com_canvasslider/js/chooser.js"></script>');
				
                // get the Data
                $form = $this->get('Form');
                $item = $this->get('Item');
                // Check for errors.
                if (count($errors = $this->get('Errors'))) 
                {
                        JError::raiseError(500, implode('<br />', $errors));
                        return false;
                }
                // Assign the Data
                $this->form = $form;
                $this->item = $item;
 
                // Set the toolbar
                $this->addToolBar();
 
                // Display the template
                parent::display($tpl);
        }
 
        /**
         * Setting the toolbar
         */
        protected function addToolBar() 
        {
                $input = JFactory::getApplication()->input;
                $input->set('hidemainmenu', true);
                $isNew = ($this->item->id == 0);
                JToolBarHelper::title($isNew ? JText::_('COM_CANVAS_SLIDER_MANAGER_SLIDERS')
                                             : JText::_('COM_CANVAS_SLIDER_MANAGER_SLIDERS'));
                JToolBarHelper::save('elementcrud.save');
                JToolBarHelper::cancel('elementcrud.cancel', $isNew ? 'JTOOLBAR_CANCEL'
                                                                   : 'JTOOLBAR_CLOSE');
        }
}