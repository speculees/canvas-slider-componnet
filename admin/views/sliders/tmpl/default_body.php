<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php foreach($this->items as $i => $item):
?>
<tr class="row<?php echo $i % 2; ?>" style="background-color: <?php echo $retVal = ($i % 2 == 0) ? '#CFF3E5' : '#FFF' ; ?> ;">
	<td><?php echo JHtml::_('grid.id', $i, $item -> id); ?></td>
	<td><?php echo JHTML::link('index.php?option=com_canvasslider&view=slider&id=' . $item -> id, $item -> name);?></td>
	<td><?php echo $item -> id; ?></td>
	<td><?php echo $item -> created; ?></td>
	<td><?php echo $item -> width; ?> x <?php echo $item -> height; ?></td>
	<td><?php echo (1 == $item -> responsive) ? 'Yes' : 'No';?></td>
	<td><?php echo (1 == $item -> fullscreen) ? 'Yes' : 'No';?></td>
</tr>
<?php endforeach; ?>