<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<tr>
	<th width="60" style="text-align: left;"><?php echo JHtml::_('grid.checkall'); ?></th>
	<th width="140" style="text-align: left;"><?php echo JText::_('COM_CANVAS_SLIDER_CANVAS_SLIDER_HEADING_SLIDER_NAME'); ?></th>
	<th><?php echo JText::_('COM_CANVAS_SLIDER_CANVAS_SLIDER_HEADING_SLIDER_ID'); ?></th>
	<th><?php echo JText::_('COM_CANVAS_SLIDER_CANVAS_SLIDER_HEADING_SLIDER_CREATED'); ?></th>
	<th><?php echo JText::_('COM_CANVAS_SLIDER_CANVAS_SLIDER_HEADING_SLIDER_SIZE'); ?></th>
	<th><?php echo JText::_('COM_CANVAS_SLIDER_CANVAS_SLIDER_HEADING_SLIDER_RESPONSIVE'); ?></th>
	<th><?php echo JText::_('COM_CANVAS_SLIDER_CANVAS_SLIDER_HEADING_SLIDER_FULLSCREEN'); ?></th>
</tr>