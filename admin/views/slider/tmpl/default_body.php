<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<ul id="sortable-list">
	<?php 
		$order = array();
		foreach($this->items as $i => $item):?>
			<li id="<?php echo $item -> id ?>">
				<div><?php echo JHtml::_('grid.id', $i, $item -> id); ?></div>
				<div><?php echo JHTML::link('index.php?option=com_canvasslider&view=elements&id=' . $item -> id, $item -> name);?></div>
				<div><?php echo $item -> id; ?></div>
				<div><?php echo $item -> time; ?></div>
			</li>
	<?php endforeach; ?>
</ul>
<input type="hidden" name="sort_order" id="sort_order" value="<?php echo implode(',',$order); ?>" />