<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<ul>
	<li><?php echo JHtml::_('grid.checkall'); ?></li>
	<li><?php echo JText::_('COM_CANVAS_SLIDER_CANVAS_SLIDER_HEADING_SLIDE_NAME'); ?></li>
	<li><?php echo JText::_('COM_CANVAS_SLIDER_CANVAS_SLIDER_HEADING_SLIDE_ID'); ?></li>
	<li><?php echo JText::_('COM_CANVAS_SLIDER_CANVAS_SLIDER_HEADING_SLIDE_TIME'); ?></li>
</ul>
