<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

class CanvasSliderViewSlider extends JViewLegacy
{
  function display($tpl=null)
  {
		$sort_order = explode ( "," , JRequest::getVar('sort_order') );
		$db = JFactory::getDbo();
		$x = 1;
 		foreach ($sort_order as $value) {
			$query = $db->getQuery(true);
			
			$fields = array($db -> quoteName('cue') . ' = ' . $db->quote($x));
			 
			// Conditions for which records should be updated.
			$conditions = array($db -> quoteName('id') . ' = ' .$value);
			 
			$query->update($db->quoteName('#__CANVAS_SLIDER_SLIDE'))->set($fields)->where($conditions);
			 
			$db->setQuery($query);
			 
			$result = $db->query();
			
			 $x++;
  		}
  }
}