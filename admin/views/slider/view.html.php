<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * CanvasSlider View
 */
class CanvasSliderViewSlider extends JViewLegacy {
	/**
	 * CanvasSlider view display method
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a JError object.
	 */
	function display($tpl = null) {

		$document = JFactory::getDocument();
		$document -> addStyleSheet('components/com_canvasslider/css/canvasslider.css');
		$document -> addCustomTag('<script type="text/javascript" src="'.JURI::base().'components/com_canvasslider/js/canvasslidermenu.js"></script>');
		$request = JRequest::get('get');

		$session = JFactory::getSession();
		$session -> set('tail', $request['id']);

		// Get data from the model
		$items = $this -> get('Items');
		$pagination = $this -> get('Pagination');

		// Check for errors.
		if (count($errors = $this -> get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign data to the view
		$this -> items = $items;
		$this -> pagination = $pagination;

		// Set the toolbar
		$this -> addToolBar();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() {
		JToolBarHelper::back('COM_CANVAS_SLIDER_BACK', 'index.php?option=com_canvasslider');
		JToolBarHelper::spacer('50');
		JToolBarHelper::title(JText::_('COM_CANVAS_SLIDER_MANAGER_SLIDERS'));
		JToolBarHelper::deleteList('', 'slider.delete');
		JToolBarHelper::editList('slidecrud.edit');
		JToolBarHelper::addNew('slidecrud.add');
	}

}
