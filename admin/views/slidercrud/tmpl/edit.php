<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
?>
<form action="<?php echo JRoute::_('index.php?option=com_canvasslider&layout=edit&id=' . (int)$this -> item -> id); ?>"
method="post" name="adminForm" id="adminForm">
	<div class="form-horizontal">
		<fieldset class="adminform">
			<legend><?php echo JText::_('COM_CANVAS_SLIDER_MANAGER_SLIDERS'); ?></legend>
			<div class="row-fluid">
				<div class="span4">
					<?php foreach($this->form->getFieldset() as $field): ?>
						<?php if ($field->label!=null) : ?>
							<div class="control-label"><?php echo $field -> label; ?></div>
						<?php endif; ?>
						<div class="controls" style="height: 40px;"><?php echo $field -> input; ?></div>
					<?php endforeach; ?>
				</div>
			</div>
		</fieldset>
	</div>
	<div>
		<input type="hidden" name="task" value="slidercrud.edit" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>