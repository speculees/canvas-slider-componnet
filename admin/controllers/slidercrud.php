<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controllerform library
jimport('joomla.application.component.controllerform');

/**
 * CanvasSlider Controller
 */
class CanvasSliderControllerSliderCrud extends JControllerForm {
	function save() {
		parent::save();
		$this -> setredirect('index.php?option=com_canvasslider');
	}
	function cancel() {
		parent::cancel();
		$this -> setredirect('index.php?option=com_canvasslider');
	}
}
