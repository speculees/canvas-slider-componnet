<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controllerform library
jimport('joomla.application.component.controllerform');

/**
 * CanvasSlider Controller
 */
class CanvasSliderControllerElementCrud extends JControllerForm {
	function save() {
		$request = JRequest::get('post');
		// If ID is '' then element is new, else it's edited
		if ($request['jform']['id'] == ''){
			
			$session = & JFactory::getSession();
			//get a db connection
			$db = JFactory::getDbo();
			$queryElement = $db -> getQuery(true);
			$queryCss = $db -> getQuery(true);
			$queryTransitions = $db -> getQuery(true);

			$columnsElement = array('name', 'content', 'alt', 'src', 'type', 'CANVAS_SLIDER_SLIDE_id');
			$valuesElement = array(
							$db -> quote($request['jform']['name']), 
							$db -> 	quote($request['jform']['content']),
							$db -> 	quote($request['jform']['alt']),
							$db -> 	quote($request['jform']['imageurl']),
							$db -> 	quote($request['jform']['select']['id']), 
							(int) $session -> get('tail', 'empty')
							);
			$queryElement
				-> insert($db -> quoteName('#__CANVAS_SLIDER_ELEMENT'))
				-> columns($db -> quoteName($columnsElement))
				-> values(implode(',', $valuesElement));

			$db -> setQuery($queryElement);
			$db -> query();
			
			$elementId = mysql_insert_id();
			
			$columnsCss = array('background', 'width', 'font', 'height', 'shadow', 'rotate', 'up', 'down', 'left', 'right', 'element_id');
			$valuesCss = array(
							$db -> 	quote($request['jform']['background']),
							(int) $request['jform']['width'], 
							$db -> 	quote($request['jform']['font']),
							(int) $request['jform']['height'],
							$db -> 	quote($request['jform']['shadow']),
							(int) $request['jform']['rotate'],
							(int) $request['jform']['upCSS'],
							(int) $request['jform']['downCSS'],
							(int) $request['jform']['leftCSS'],
							(int) $request['jform']['rightCSS'],
							(int) $elementId,
							);
							
			$queryCss
				-> insert($db -> quoteName('#__CANVAS_SLIDER_CSS'))
				-> columns($db -> quoteName($columnsCss))
				-> values(implode(',', $valuesCss));
				
			$db -> setQuery($queryCss);
			$db -> query();
			
			$columnsTransitions = array('up', 'down', 'left', 'right', 'fxTransition', 'fxEase', 'duration', 'delay', 'element_id');
			$valuesTransitions = array(
							(int) $request['jform']['upTRANSIT'],
							(int) $request['jform']['downTRANSIT'],
							(int) $request['jform']['leftTRANSIT'],
							(int) $request['jform']['rightTRANSIT'],
							$db -> 	quote($request['jform']['fxTransition']),
							$db -> 	quote($request['jform']['fxEase']),
							(int) $request['jform']['duration'],
							(int) $request['jform']['delay'],
							(int) $elementId,
							);
							
			$queryTransitions
				-> insert($db -> quoteName('#__CANVAS_SLIDER_TRANSITIONS'))
				-> columns($db -> quoteName($columnsTransitions))
				-> values(implode(',', $valuesTransitions));
				
			$db -> setQuery($queryTransitions);
			$db -> query();
			
			$this -> setredirect('index.php?option=com_canvasslider&view=elements&id=' . $session -> get('tail', 'empty'));
		} else {
			$db = JFactory::getDbo();
			$query = $db -> getQuery(true);
			
			//Fields to update
			$fields = array(
							$db->quoteName('name'). '=' . $db->quote($request['jform']['name']),
							$db->quoteName('time'). '=' . $db->quote($request['jform']['time'])
						);			
			//Conditions for wich records should be updated
			$conditions = array(
				$db->quoteName('id') . '=' . (string)$request['jform']['id']
			);
			$query->update($db->quoteName('#__CANVAS_SLIDER_SLIDE'))->set($fields)->where($conditions);
			$db -> setQuery($query);
			$db -> query();

			$session = & JFactory::getSession();
			$this -> setredirect('index.php?option=com_canvasslider&view=slider&id=' . $session -> get('tail', 'empty'));
		}
	}

	function cancel() {
		 parent::cancel();
		 $session = & JFactory::getSession();
		 $this -> setredirect('index.php?option=com_canvasslider&view=elements&id=' . $session -> get('tail', 'empty'));
	}
	
}
