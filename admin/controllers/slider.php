<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controlleradmin library
jimport('joomla.application.component.controlleradmin');

/**
 * CanvasSlider Controller
 */
class CanvasSliderControllerSlider extends JControllerAdmin {
	/**
	 * Proxy for getModel. Dodato  $config=array('ignore_request' => true) u getModel
	 * @since       2.5
	 */
	public function getModel($name = 'SlideCrud', $prefix = 'CanvasSliderModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	public function delete() {
		parent::delete();
		$session =& JFactory::getSession();
		$this -> setredirect('index.php?option=com_canvasslider&view=slider&id='.$session->get( 'tail', 'empty' ));
	}
}
