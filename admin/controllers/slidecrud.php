<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controllerform library
jimport('joomla.application.component.controllerform');

/**
 * CanvasSlider Controller
 */
class CanvasSliderControllerSlideCrud extends JControllerForm {
	function save() {
		$request = JRequest::get('post');
		if ($request['jform']['id'] == ''){
			$session = & JFactory::getSession();
			//get a db connection
			$db = JFactory::getDbo();
			$query = $db -> getQuery(true);

			$columns = array('name', 'time', 'CANVAS_SLIDER_id');
			$values = array(
							$db -> quote($request['jform']['name']), 
							(int) $request['jform']['time'], 
							(int) $session -> get('tail', 'empty')
							);
			$query
				-> insert($db -> quoteName('#__CANVAS_SLIDER_SLIDE'))
				-> columns($db -> quoteName($columns))
				-> values(implode(',', $values));

			$db -> setQuery($query);
			$db -> query();
			$this -> setredirect('index.php?option=com_canvasslider&view=slider&id=' . $session -> get('tail', 'empty'));
		} else {
			$db = JFactory::getDbo();
			$query = $db -> getQuery(true);
			
			//Fields to update
			$fields = array(
							$db->quoteName('name'). '=' . $db->quote($request['jform']['name']),
							$db->quoteName('time'). '=' . $db->quote($request['jform']['time'])
						);			
			//Conditions for wich records should be updated
			$conditions = array(
				$db->quoteName('id') . '=' . (string)$request['jform']['id']
			);
			$query->update($db->quoteName('#__CANVAS_SLIDER_SLIDE'))->set($fields)->where($conditions);
			$db -> setQuery($query);
			$db -> query();

			$session = & JFactory::getSession();
			$this -> setredirect('index.php?option=com_canvasslider&view=slider&id=' . $session -> get('tail', 'empty'));
		}
	}

	function cancel() {
		 parent::cancel();
		 $session = & JFactory::getSession();
		 $this -> setredirect('index.php?option=com_canvasslider&view=slider&id=' . $session -> get('tail', 'empty'));
	}
}
