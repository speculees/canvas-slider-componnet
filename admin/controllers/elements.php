<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controlleradmin library
jimport('joomla.application.component.controlleradmin');

/**
 * CanvasSlider Controller
 */
class CanvasSliderControllerElements extends JControllerAdmin {
	
	
	/**
	 * Proxy for getModel. Dodato  $config=array('ignore_request' => true) u getModel
	 * @since       2.5
	 */
	public function getModel($name = 'ElementCrud', $prefix = 'CanvasSliderModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
	
	public function delete(){
		parent::delete();
		$session =& JFactory::getSession();
		$this -> setredirect('index.php?option=com_canvasslider&view=elements&id='.$session->get( 'tail', 'empty' ));
	}
	
	public function deleteElement($value, $table){
		$db = JFactory::getDbo();	
		$query = $db -> getQuery(true);
		
		$conditions = array($db -> quoteName('id') . ' = ' .$value);

		$query -> delete($db -> quoteName($table));
		$query -> where($conditions);
		
		$db -> setQuery($query);

		$result = $db -> query();
	}
}
