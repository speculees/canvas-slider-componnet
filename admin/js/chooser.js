var Slider = new Class({
  initialize: function(name, type){
    this.container = document.id(name);
    if (type == 'horizontal'){
      this.direction = ['left','right'];
    }else if(type == 'vertical'){
      this.direction = ['top','bottom'];
    }else{
      alert('type can only be "horizontal" or "vertical"');
    }
    this.list = this.container.getElement('ul').addEvents({
      'mousewheel' : function(event){
       /* Mousewheel UP */
      if (event.wheel > 0 && !this.busy){
        this.forward.fireEvent('click');
      }
      /* Mousewheel DOWN*/
      else if (event.wheel < 0 && !this.busy){
        this.backward.fireEvent('click');
      }
      event.preventDefault();
    }.bind(this),
      'mouseenter' : function(event){
        //event.preventDefault();
      }});
    this.items = this.list.getChildren('li');
    this.busy = false;
    this.initr = false; 
    this.forward = this.container.getElement('.btnf').addEvent('click', function(){
      if(!this.initr){
        this.initiate(this.direction[1]);
      }
      if(!this.busy){
        this.items = this.list.getChildren('li');
        this.list.tween(this.direction[0], [-53, 0]);
      }
    }.bind(this));
    this.backward = this.container.getElement('.btnb').addEvent('click', function(){
      if(this.initr){
        this.initiate(this.direction[0]);
      }
      if(!this.busy){
        this.list.tween(this.direction[0], [0, -53]);
        this.items = this.list.getChildren('li');
      }
    }.bind(this));
    //create initiated object
    this.initiate(this.direction[0]);
    this.list.getElements('li').forEach(function(element){
      element.addEvent('click', function(event){
        var parentItem = element.getParent().getParent();
        if(parentItem.tagName == 'DIV'){
          this.list.getChildren('li').forEach(function(e){
            e.setStyle('background-color', '#AAA');
          });
          element.setStyle('background-color', '#066');
        }else{
          parentItem.appendChild(element.getElement('span'));
          element.appendChild(parentItem.getFirst('span'));
          parentItem.fireEvent('click');
        }
        return false;
      }.bind(this));
    }.bind(this));
  },
  initiate : function(direction){
    this.list.set('tween', {
      transition: 'expo:in',
      duration: '100',
      onComplete: function() {
        if(direction == this.direction[0]){
            this.items[0].inject(this.items.getLast(), 'after');
            this.list.setStyle(this.direction[0], 0);
        }
        this.busy = false;      
      }.bind(this),
      onStart: function(){
        if(direction == this.direction[1]){
          this.items.getLast().inject(this.items[0], 'before');
          this.list.setStyle(this.direction[0], -53);
          this.initr = true;
        }else{
          this.initr = false;
        }
        this.busy = true;
      }.bind(this)
    });
  },
  setParent : function(el, newParent) {
    newParent.appendChild(el);
  }
});
