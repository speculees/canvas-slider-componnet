window.addEvent('domready', function(){

  $$('#draggable').makeDraggable({

    droppables: $$('.position'),

    onEnter: function(draggable, droppable){
      droppable.setStyle('background', '#E79D35');
    },

    onLeave: function(draggable, droppable){
      droppable.setStyle('background', '#6B7B95');
    },

    onDrop: function(draggable, droppable){
      
    }
  });
});
