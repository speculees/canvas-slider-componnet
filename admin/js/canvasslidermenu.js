/* when the DOM is ready */
window.addEvent('domready', function() {
	/* grab important elements */
	var sortInput = document.id('sort_order');

	var messageBox = document.id('message-box');
	var list = document.id('sortable-list');

	/* get the request object ready;  re-use the same Request */
	var url = "index.php?option=com_canvasslider&view=slider&format=ajax";
	
	var request = new Request({
		url: url,
		link: 'cancel',
		method: 'post',
		onRequest: function() {
			messageBox.set('text','Updating the sort order in the database.');
		},
		onSuccess: function() {
			messageBox.set('text','Database has been updated.');
		}
	});
	/* worker function */
	var fnSubmit = function() {
		var sortOrder = [];
		list.getElements('li').each(function(li) {
			if (li.getProperty('id') != null) {
				sortOrder.push(li.getProperty('id'));
			}

		});
		console.log(sortOrder);
		sortInput.value = sortOrder.join(',');
		request.send('sort_order=' + sortInput.value);
	};
	
	/* store values */
	list.getElements('li').each(function(li) {
		li.store('id',li.get('title')).set('title','');
	});
	
	/* sortables that also *may* */
	new Sortables(list,{
		constrain: true,
		clone: true,
		revert: true,
		onComplete: function(el,clone) {
			fnSubmit();
		}
	});
	
	/* ajax form submission */
	document.id('adminForm').addEvent('submit',function(e) {
		if(e) e.stop();
		fnSubmit();
	});
	
	
});