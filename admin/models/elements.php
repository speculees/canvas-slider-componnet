<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelform library
jimport('joomla.application.component.modeladmin');

/**
 * CanvasSlider Model
 */
class CanvasSliderModelElements extends JModelList {
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	**/
	protected function getListQuery() {
		$id = JRequest::getVar('id');
		$db = JFactory::getDBO();
		$query = $db -> getQuery(true);
		$query 	-> select('id,name,cue,type')
				-> from('#__CANVAS_SLIDER_ELEMENT')
				-> where('CANVAS_SLIDER_SLIDE_id = '. (int) $id)
				-> order($db->escape($this->getState('list.ordering', 'cue')).' '. $db->escape($this->getState('list.direction', 'ASC')));
		return $query;
	}
}
