<?php
// No direct access to this file
defined('_JEXEC') or die ;

// import the list field type
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

/**
 * HelloWorld Form Field class for the HelloWorld component
 */
class JFormFieldElementCrud extends JFormFieldList {
	/**
	 * The field type.
	 *
	 * @var         string
	 */
	protected $type = 'ElementCrud';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return      array           An array of JHtml options.
	 */
	protected function getOptions() {
		$messages = array('Image','Text','Canvas');	
		$options = array();
		foreach ($messages as $key => $message) {
			$options[] = JHtml::_('select.option', strtolower($message[0]), $message);
		}
		$options = array_merge(parent::getOptions(), $options);
		return $options;
	}
}
