<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla modelform library
jimport('joomla.application.component.modeladmin');
 
/**
 * CanvasSlider Model
 */
class CanvasSliderModelElementCrud extends JModelAdmin
{
        /**
         * Returns a reference to the a Table object, always creating it.
         *
         * @param       type    The table type to instantiate
         * @param       string  A prefix for the table class name. Optional.
         * @param       array   Configuration array for model. Optional.
         * @return      JTable  A database object
         * @since       2.5
         */
        public function getTable($type = 'Element', $prefix = 'CanvasSliderTable', $config = array()) 
        {
			return JTable::getInstance($type, $prefix, $config);
        }
        /**
         * Method to get the record form.
         *
         * @param       array   $data           Data for the form.
         * @param       boolean $loadData       True if the form is to load its own data (default case), false if not.
         * @return      mixed   A JForm object on success, false on failure
         * @since       2.5
         */
        public function getForm($data = array(), $loadData = true) 
        {
                // Get the form.
                $form = $this->loadForm('com_canvasslider.elementcrud', 'elementcrud',
                                        array('control' => 'jform', 'load_data' => $loadData));
                if (empty($form)) 
                {
                        return false;
                }
                return $form;
        }
        /**
         * Method to get the data that should be injected in the form.
         *
         * @return      mixed   The data for the form.
         * @since       2.5
         */
        protected function loadFormData() {
			return $this -> getItem();
		}
	
		public function getItem($pk = null) {
			$pk = (!empty($pk)) ? $pk : (int)$this -> getState($this -> getName() . '.id');
			
				$table = $this -> getTable();
				$table -> load($pk);

				$properties = $table -> getProperties(1);
				
			if ($pk != null){

				$cssTable = JModelAdmin::getInstance( 'CSS', 'CanvasSliderModel' )-> getTable();
				$cssTable->bind($cssTable -> loadDataByElementId($pk));
				
				$transitionsTable = JModelAdmin::getInstance( 'Transitions', 'CanvasSliderModel' )-> getTable();
				$transitionsTable->bind($transitionsTable -> loadDataByElementId($pk));
				
				$cssProperties = $cssTable -> getProperties(1);
				$transitionsProperties = $transitionsTable -> getProperties(1);
				
				$result = array_merge($properties, $cssProperties, $transitionsProperties);
				$item = JArrayHelper::toObject($result, 'JObject');		
			} else {
				$item = JArrayHelper::toObject($properties, 'JObject');
			}
			
			return $item;
		}
}
