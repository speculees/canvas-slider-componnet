<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
 
// import Joomla table library
jimport('joomla.database.table');
 
/**
 * CanvasSlider Table class
 */
class CanvasSliderTableTransitions extends JTable
{
        /**
         * Constructor
         *
         * @param object Database connector object
         */
        function __construct(&$db) 
        {
                parent::__construct('#__CANVAS_SLIDER_TRANSITIONS', 'id', $db);
        }
		
		function loadDataByElementId($id) 
		{
			$db = JFactory::getDbo();
			$query = $db -> getQuery(true);
			$query -> select('*') -> 
				from('#__CANVAS_SLIDER_TRANSITIONS') -> where('element_id = ' . $id);
			$db -> setQuery($query);
			$result = $db -> loadObject();
			return $result;
		}
}