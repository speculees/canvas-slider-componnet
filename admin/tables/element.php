<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
 
// import Joomla table library
jimport('joomla.database.table');
 
/**
 * CanvasSlider Table class
 */
class CanvasSliderTableElement extends JTable
{
        /**
         * Constructor
         *
         * @param object Database connector object
         */
        function __construct(&$db) 
        {
                parent::__construct('#__CANVAS_SLIDER_ELEMENT', 'id', $db);
        }
}